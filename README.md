# NRPE service for Nagios checks


This Ansible role installs the [Nagios Remote Command Executor](https://support.nagios.com/kb/article/nrpe-agent-and-plugin-explained-612.html),
a daemon allowing Nagios to perform local checks on Unix servers. 

## Installation

Add the following to a `requirements.yml` or `requirements.yaml` file in your
Ansible project and run `ansible-galaxy install -r requirements.yaml`:

```yaml
collections:
  - community.general

roles:
  - name: nrpe
    scm: git
    src: https://gitlab.uvm.edu/saa/ansible-galaxy/nrpe.git
```

## Usage

Place a section like this in your playbook:

```yaml
- name: Set up nrpe with standard checks
  hosts: all
  roles:
    - role: nrpe
```

Additional checks can be configured during setup:

```yaml
- name: Set up nrpe with custom checks
  hosts: all
  roles:
    - role: nrpe
      nrpe_custom_commands:
        check_nginx_processes: "/usr/lib64/nagios/plugins/check_procs -C nginx -c 1:"
```

### Additional options

Full argument documentation is available via `ansible-doc -t role nrpe`.